# Holdpass

Because password reuse is a terrible habit that leaves users open to being hacked, password lockers are vital apps in today's world. Holdpass is a password locker application built on Torty. It can be used as a plugin for any site built with Torty, or it can be used as a standalone web application. All passwords are encrypted before being stored.
